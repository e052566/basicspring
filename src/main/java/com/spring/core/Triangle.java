package com.spring.core;

import java.util.ArrayList;
import java.util.List;

public class Triangle {

	private List<Point> points=new ArrayList<Point>();
	
		public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

		public void draw() {

			System.out.println("Triangle co ordinates are : " );
			for(Point pnt:points)
			{

				System.out.println("("+pnt.getX()+","+pnt.getY()+")" );
			}
		

	}

}
